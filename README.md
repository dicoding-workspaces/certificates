# Dicoding Academy Certificates

This repository stores all my earned certificates from Dicoding Academy courses, specifically those related to the Frontend Backend Path.

## Contributing

This repository is currently private and not open for external contributions.

## License

The contents of this repository are not publicly licensed and are intended for personal use only.
